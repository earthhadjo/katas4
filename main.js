const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function displayResults(functionName, input) {
    const katas = document.getElementById("katas");
    const header = document.createElement("h2");
    header.textContent = functionName;
    const newElement = document.createElement("div");
    newElement.textContent = input;
    document.body.appendChild(header);
    document.body.appendChild(newElement);
}
function katas1() {
    displayResults("Katas1", JSON.stringify(gotCitiesCSV.split(","))
    );

    return gotCitiesCSV

}
katas1()

function katas2() {
    displayResults("Katas2", JSON.stringify(bestThing.split(" ")));
    return bestThing


}
katas2()

function katas3() {
    let CSVarray = gotCitiesCSV.split(",")
    displayResults("Katas3", JSON.stringify(CSVarray.join("; ")));
    return gotCitiesCSV

}
katas3()

function katas4() {
    displayResults("Katas4", JSON.stringify(lotrCitiesArray.toString()));
    return lotrCitiesArray

}
katas4()

function katas5() {
    displayResults("Katas5", JSON.stringify(lotrCitiesArray.slice(0, 5)));
    return lotrCitiesArray
}
katas5()

function katas6() {
    displayResults("Katas6", JSON.stringify(lotrCitiesArray.slice(3)));
    return lotrCitiesArray

}
katas6()

function katas7() {
    displayResults("Katas7", JSON.stringify(lotrCitiesArray.slice(2, 5)));
    return lotrCitiesArray

}
katas7()

function katas8() {
    lotrCitiesArray.splice(2, 1);
    displayResults("Katas8", JSON.stringify(lotrCitiesArray));
    return lotrCitiesArray

}
katas8()

function katas9() {
    lotrCitiesArray.splice(5, 2)
    displayResults("Katas9", JSON.stringify(lotrCitiesArray));
    return lotrCitiesArray

}
katas9()

function katas10() {
    lotrCitiesArray.splice(2, 0, "Rohan")
    displayResults("Katas10", JSON.stringify(lotrCitiesArray));
    return lotrCitiesArray


}
katas10()

function katas11() {
    lotrCitiesArray.splice(5, 1, "Deadest Marshes")
    displayResults("Katas11", JSON.stringify(lotrCitiesArray));
    return lotrCitiesArray
}

katas11()

function katas12() {
    displayResults("Katas12", JSON.stringify(bestThing.slice(0, 14)));
    return bestThing

}
katas12()

function katas13() {
    displayResults("Katas13", JSON.stringify(bestThing.slice(-12)));
    return bestThing
}
katas13()

function katas14() {
    displayResults("Katas14", JSON.stringify(bestThing.slice(23, 38)));
    return bestThing

}
katas14()

function katas15() {
    displayResults("Katas15", JSON.stringify(bestThing.substring(bestThing.length - 12)));
    return bestThing
}
katas15()

function katas16() {
    displayResults("Katas16", JSON.stringify(bestThing.substring(23, 38)));
    return bestThing
}
katas16()

function katas17() {
    displayResults("Katas17", JSON.stringify(bestThing.indexOf("only")));
    return bestThing
}
katas17()

function katas18() {
    displayResults("Katas18", JSON.stringify(bestThing.indexOf("bit")));
    return bestThing
}
katas18()

function katas19() {
    let citiesarray = gotCitiesCSV.split(",")
    let resultsarray = []
    let doublevowels = /aa|ee|ii|oo|uu/g
    for (let cities of citiesarray) {
        if (doublevowels.test(cities)) {
            resultsarray.push(cities)
        }
    }
    displayResults("Katas19", JSON.stringify(resultsarray));
    return resultsarray
}
katas19()

function find(arr,query){
    return arr.filter(function(el){
        return el.toLowerCase().indexOf(query.toLowerCase()) !== -1;
    })
}

function katas20() {
    let solution = find(lotrCitiesArray, "or")
    displayResults("Katas20", JSON.stringify(solution));
    return lotrCitiesArray

}
katas20()

function katas21(){
    let bestThingArray = bestThing.split(" ")
    let result = []
    for (let words of bestThingArray) {
        if(words.startsWith("b") === true) {
            result.push(words)
        } 
    }
    console.log(result)
    
    displayResults("Katas21", JSON.stringify(result));
}
katas21()

function katas22(){

    if (lotrCitiesArray.includes("Mirkwood") ===true) {
    displayResults("Katas22", "Yes")
    return lotrCitiesArray
}
    else {
    displayResults("Katas22", "No")
    return lotrCitiesArray

}
}
katas22()

function katas23(){
    
    if (lotrCitiesArray.includes("Hollywood") ===true) {
    displayResults("Katas23", "Yes")
    return lotrCitiesArray
}

        else {
        displayResults("Katas23", "No")
        return lotrCitiesArray
    
}
}
katas23()  

function Katas24(){
  let result = lotrCitiesArray.indexOf("Mirkwood")
  displayResults("Katas24", JSON.stringify(result)); 
  return lotrCitiesArray

}
Katas24()

function Katas25(){
    let check = " "
    let result = []
    for (let city of lotrCitiesArray){
        if (city.includes(check) === true) {
          result.push(city)
        }     
    
    }
    displayResults("Katas25", JSON.stringify(result))
    return lotrCitiesArray
}
Katas25()

function Katas26(){
displayResults("Katas26", JSON.stringify(lotrCitiesArray.reverse()))

}
Katas26()

function Katas27(){
    displayResults("Katas27", JSON.stringify(lotrCitiesArray.sort()))
}

Katas27()

function Katas28(){
    let charNum = []
    lotrCitiesArray.sort(function(a, b){
        return a.length - b.length
    })
    console.log(lotrCitiesArray) 
    displayResults("Katas28", JSON.stringify(lotrCitiesArray))
}
Katas28()

function Katas29(){
    lotrCitiesArray.pop()
    displayResults("Katas29", JSON.stringify(lotrCitiesArray))
}
Katas29()

function Katas30(){
    lotrCitiesArray.push("Deadest Marshes")
    displayResults("Katas30", JSON.stringify(lotrCitiesArray))

}
Katas30()

function Katas31(){
    lotrCitiesArray.shift()
    displayResults("Katas31", JSON.stringify(lotrCitiesArray))
}
Katas31()

function Katas32(){
    lotrCitiesArray.unshift("Rohan")
    displayResults("Katas32", JSON.stringify(lotrCitiesArray))
}
Katas32()